/*
 * RFM69Morse.cpp
 *
 *  Created on: Apr 1, 2017
 *      Author: roland
 */

#include "RFM69Morse.h"

#include <Arduino.h>

static const uint8_t RFM69Morse::_specialCount = 16;

const char* RFM69Morse::_letters[] = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
const char* RFM69Morse::_digits[] = {"-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----."};
const RFM69Morse::SpecialChar RFM69Morse::_special[RFM69Morse::_specialCount] = {
		{'.', ".-.-.-"},
		{',', "--..--"},
		{'\'', ".----."},
		{'"', ".-..-."},
		{'_', "..--.-"},
		{':', "---..."},
		{';', "-.-.-."},
		{'?', "..--.."},
		{'!', "-.-.--"},
		{'-', "-....-"},
		{'+', ".-.-."},
		{'/', "-..-."},
		{'(', "-.--."},
		{')', "-.--.-"},
		{'=', "-...-"},
		{'@', ".--.-."}
};

RFM69Morse::RFM69Morse(uint8_t sck, uint8_t miso, uint8_t mosi, uint8_t nss, uint16_t diTime)
: _sck(sck), _miso(miso), _mosi(mosi), _nss(nss), _diTime(diTime), _dahTime(diTime * 3), _spaceTime(diTime), _letSpaceTime(diTime * 2), _wordSpaceTime(diTime * 4)
{
	pinMode(_sck, OUTPUT);
	pinMode(_miso, INPUT);
	pinMode(_mosi, OUTPUT);
	pinMode(_nss, OUTPUT);

	digitalWrite(_nss, HIGH);

//	writeReg(0x07, 0x6c);	//set freq to 433MHz
//	writeReg(0x08, 0x40);	//set freq to 433MHz
//	writeReg(0x09, 0x00);	//set freq to 433MHz

	writeReg(0x02, 3 << 5);	//continuous mode wo bit sync
	writeReg(0x11, 0x00);		//low power
	writeReg(0x01, 3 << 2);	//transmitter mode
	writeReg(0x13, 0x0f);	//over current protection setting for high power
	writeReg(0x5a, 0x5d);	//high power
	writeReg(0x5c, 0x7c);	//high power
}

void RFM69Morse::setFreq(uint32_t freq)
{
//	writeReg(0x01, 4 << 2);	//rx mode

	freq /= 61.03515625;

	bool done;
	do	//for some reason my module not always set freq after first attempt...
	{
		done = true;
		writeReg(0x09, freq);
		writeReg(0x08, freq >> 8);
		writeReg(0x07, freq >> 16);

		if(readReg(0x09) != static_cast<uint8_t>(freq))
			done = false;
		if(readReg(0x08) != static_cast<uint8_t>(freq >> 8))
			done = false;
		if(readReg(0x07) != static_cast<uint8_t>(freq >> 16))
			done = false;
	}while(!done);
}

void RFM69Morse::writeReg(uint8_t reg, uint8_t val)
{
	reg |= 0x80;	//MSB in address is W/!R

	digitalWrite(_nss, LOW);

	shiftOut(_mosi, _sck, MSBFIRST, reg);
	shiftOut(_mosi, _sck, MSBFIRST, val);

	digitalWrite(_nss, HIGH);
}

uint8_t RFM69Morse::readReg(uint8_t reg)
{
	reg &= ~(0x80);	//MSB in address is W/!R

	digitalWrite(_nss, LOW);

	shiftOut(_mosi, _sck, MSBFIRST, reg);
	uint8_t ret;
	ret = shiftIn(_miso, _sck, MSBFIRST);

	digitalWrite(_nss, HIGH);

	return ret;
}

void RFM69Morse::sendString(const char* string)
{
    while(*string != '\0')
    {
        char letter = tolower(*string);

        if(letter == ' ')
        {
        	delay(_wordSpaceTime);
        }
        else if(letter >= 'a' && letter <= 'z')
        {
            send(_letters[letter - 'a']);
        }
        else if(letter >= '0' && letter <= '9')
        {
            send(_digits[letter - '0']);
        }
        else
        {
        	for(uint8_t i = 0; i < _specialCount; ++i)
        	{
        		if(_special[i].ch == letter)
        		{
        			send(_special[i].morse);
        			break;
        		}
        	}
        }

        ++string;
    }
}

void RFM69Morse::send(const char* morse)
{
    while(*morse != '\0')
    {
        if(*morse == '.')
            sendDi();
        else
            sendDah();

        ++morse;
    }
    delay(_letSpaceTime);
}

void RFM69Morse::sendDi(void)
{
    keyDown();
    delay(_diTime);
    keyUp();
    delay(_spaceTime);
}

void RFM69Morse::sendDah(void)
{
    keyDown();
    delay(_dahTime);
    keyUp();
    delay(_spaceTime);
}

void RFM69Morse::keyDown(void)
{
	writeReg(0x11, 0x7f);
}

void RFM69Morse::keyUp(void)
{
	writeReg(0x11, 0x00);
}
