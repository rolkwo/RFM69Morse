/*
 * RFM69Morse.h
 *
 *  Created on: Apr 1, 2017
 *      Author: roland
 */

#ifndef RFM69MORSE_H_
#define RFM69MORSE_H_

#include <Arduino.h>

class RFM69Morse
{
public:
	RFM69Morse(uint8_t sck, uint8_t miso, uint8_t mosi, uint8_t nss, uint16_t diTime);

	// frequency in Hz
	void setFreq(uint32_t freq);

	void sendString(const char* string);

	void writeReg(uint8_t reg, uint8_t val);
	uint8_t readReg(uint8_t reg);
private:
	uint8_t _sck;
	uint8_t _miso;
	uint8_t _mosi;
	uint8_t _nss;

    static const uint8_t _specialCount;

    struct SpecialChar
	{
    	char ch;
    	const char* morse;
	};

    void send(const char* morse);
    void sendDi(void);
    void sendDah(void);
    void keyDown(void);
    void keyUp(void);

    static const char* _letters[];
    static const char* _digits[];
    static const SpecialChar _special[];

    uint16_t _diTime;
    uint16_t _dahTime;
    uint16_t _spaceTime;
    uint16_t _letSpaceTime;
    uint16_t _wordSpaceTime;
};



#endif /* RFM69MORSE_H_ */
