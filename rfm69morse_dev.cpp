#include <Arduino.h>

#include "RFM69Morse.h"

RFM69Morse morse(7, 8, 9, 10, 100);

void setup() {
	Serial.begin(9600);
}

void loop() {
//	morse.writeReg(0x07, 0x6c);
//	morse.writeReg(0x08, 0x40);
//	morse.writeReg(0x09, 0x00);
	morse.setFreq(432450000);
//	morse.writeReg(0x02, 3 << 5);	//continuous mode wo bit sync
//	morse.writeReg(0x11, 0xe0);		//full power
//	morse.writeReg(0x01, 3 << 2);	//transmitter mode
//	morse.setFreq(432450000);
//	for(uint8_t i = 0; i < 0xf; ++i)
//	{
//		morse.writeReg(0x11, 0xe0);		//full power
//		delay(1000);
//		morse.writeReg(0x11, 0x80);		//low power
//		delay(1000);
//	}

	while(true)
	{
		morse.sendString("vvv vvv vvv sq9rak sq9rak sq9rak jo90wa jo90wa po 100mw mail sq9rak@gmail.com vy 73 de sq9rak");
		delay(1000);
	}
}
